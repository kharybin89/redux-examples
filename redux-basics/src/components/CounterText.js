import React, { Component, PropTypes } from 'react'

class CounterText extends Component {
  render() {
    const { count } = this.props
    const style = {
      color: count % 2 === 0 ? 'green' : 'red'
    }

    return <p style={style}>Clicks count: {count}</p>
  }
}

CounterText.propTypes = {
  count: PropTypes.number.isRequired
}

export default CounterText

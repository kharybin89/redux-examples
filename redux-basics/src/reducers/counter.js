import { COUNTER_INCREMENT } from '../actions'

const initialState = {
  count: 0
}

export default (state = initialState, action) => {
  switch (action.type) {
    case COUNTER_INCREMENT:
      return { count: state.count + 1 }
    default:
      return state
  }
}
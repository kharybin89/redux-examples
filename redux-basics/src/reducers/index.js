import { combineReducers } from 'redux'
import counter from './counter'

console.log(counter)
const rootReducer = combineReducers({
  counter
})

export default rootReducer

import React, { Component, PropTypes } from 'react'
import { Provider } from 'react-redux'
import Counter from './Counter'

const Root = ({ store }) => (
  <Provider store={store}>
    <Counter />
  </Provider>
)

Root.propTypes = {
  store: PropTypes.object.isRequired
}

export default Root

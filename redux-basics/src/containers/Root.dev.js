import React, { PropTypes } from 'react'
import { Provider } from 'react-redux'
import Counter from './Counter'
import DevTools from './DevTools'

const Root = ({ store }) => (
  <Provider store={store}>
    <div>
      <Counter />
      <DevTools />
    </div>
  </Provider>
)

Root.propTypes = {
  store: PropTypes.object.isRequired
}

export default Root

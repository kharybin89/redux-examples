import React, { Component } from 'react'
import CounterText from '../components/CounterText'
import { increment } from '../actions'
import { connect } from 'react-redux'

class Counter extends Component {
  render() {
    return (
      <div>
        <CounterText count={this.props.count} />
        <button onClick={this.props.increment}>Increment</button>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    increment: () => dispatch(increment())
  }
}

const mapStateToProps = (state) => {
  return {
    count: state.counter.count
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter)

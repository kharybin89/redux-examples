import React, { Component } from 'react'
import CounterText from './CounterText'

class Counter extends Component {
  state = {
    count: 0
  }

  increment() {
    this.setState({ count: this.state.count + 1 })
  }

  render() {
    return (
      <div>
        <CounterText count={this.state.count}/>
        <button onClick={this.increment.bind(this)}>Increment</button>
      </div>
    )
  }
}

export default Counter
